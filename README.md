# <img src="https://www.logolynx.com/images/logolynx/1b/1bcc0f0aefe71b2c8ce66ffe8645d365.png"  width="30" height="30"> A Discord bot made for great French communities

## Features
- Captain Coaster API
- Captain Coaster Quizz
- Custom Error handler
- Poll
- Dev features (on the fly embeds, ...)
- Youtube song to voice channel
- ...

## Discord servers using this bot
- r/mkfr
- Coasters World
- HSFactory