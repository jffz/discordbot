import logging
import random
import re
import inflect

import discord
from discord.ext import commands
from discord.ext.commands import Context
import bot.utils.emojis as emojis

log = logging.getLogger(__name__)

p = inflect.engine()
intemojis = emojis.emojis_numbers()


class Poll(commands.Cog, name='Poll Cog'):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='poll', aliases=['vote'])
    @commands.guild_only()
    async def poll(self, ctx: Context, *args: commands.clean_content):
        """
        /poll "Question ?" "Choice 1" "Choice 2" "Choice 3"
        """
        try:
            await ctx.message.delete()
        except Exception as e:
            log.info("No permission to delete messages")

        # Transform tuple args to list so we can pop items
        argslist = list(args)
        if len(argslist) < 3 or len(argslist) > 11:
            log.info('Not enough or too much options (3 to 11 args required)')
            return

        embed = discord.Embed(
            title=f'[Poll] {argslist.pop(0).capitalize()}',
            colour=discord.Colour.dark_gold())
        embed.set_author(
            icon_url=ctx.author.avatar_url,
            name=ctx.author.name,
            url=ctx.author.avatar_url)

        # Link emojis to choices
        for i, opt in enumerate(argslist):
            embed.add_field(
                name=f'{intemojis[p.number_to_words(i)]} {opt} (0)',
                value=" ‏‏‎ ", inline=False)

        # Send embed and add reaction to embed
        message = await ctx.send(content='', embed=embed)
        for i, opt in enumerate(argslist):
            await message.add_reaction(intemojis[p.number_to_words(i)])

        log.info(f"{ctx.author} started a poll: {args}")


    @commands.Cog.listener()
    @commands.guild_only()
    async def on_reaction_add(self, react: discord.Reaction, person: discord.User):
        """
        Update poll on reaction add
        """
        if person == self.bot.user:
            return

        msg = react.message
        mbd = msg.embeds[0]
        if msg.author == self.bot.user and mbd:
            if '[Poll]' not in mbd.title:
                return

            embed = discord.Embed(
                title=mbd.title,
                colour=mbd.colour)

            regex = re.compile('\(.*\)')
            for i, field in enumerate(mbd.fields):
                field_value = regex.sub('', field.name[3:]).replace(' ', '')
                for r in msg.reactions:
                    if int(r.emoji[0]) == i:
                        embed.add_field(
                            name=f'''{intemojis[p.number_to_words(i)]} {field_value} ({r.count - 1})''',
                            value=" ‏‏‎ ", inline=False)

            await react.message.edit(embed=embed)


    @commands.Cog.listener()
    @commands.guild_only()
    async def on_reaction_remove(self, react: discord.Reaction, person: discord.User):
        """
        Update poll on reaction add
        """
        if person == self.bot.user:
            return

        msg = react.message
        mbd = msg.embeds[0]
        if msg.author == self.bot.user and mbd:
            if '[Poll]' not in mbd.title:
                return

            embed = discord.Embed(
                title=mbd.title,
                colour=mbd.colour)

            regex = re.compile('\(.*\)')
            for i, field in enumerate(mbd.fields):
                field_value = regex.sub('', field.name[3:]).replace(' ', '')
                for r in msg.reactions:
                    if int(r.emoji[0]) != i:
                        continue
                    embed.add_field(
                        name=f'''{intemojis[p.number_to_words(i)]} {field_value} ({r.count - 1})''',
                        value=" ‏‏‎ ", inline=False)

            await react.message.edit(embed=embed)


def setup(bot):
    bot.add_cog(Poll(bot))
